// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDdIZU-Se8haLnbcsQ1T84VE7hS880mtZs",
  authDomain: "push-ec4db.firebaseapp.com",
  projectId: "push-ec4db",
  storageBucket: "push-ec4db.appspot.com",
  messagingSenderId: "693071420429",
  appId: "1:693071420429:web:041af3a589a362348e4944",
  measurementId: "G-FNDE8JTC35"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);