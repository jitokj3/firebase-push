import './App.css';
import { initializeApp } from "firebase/app";
import { getMessaging, getToken } from "firebase/messaging";
import React, { useEffect } from 'react';

// sample token - 
// token - f3cbWKa1J1tb5pF3n0kbJ9:APA91bHoeSPEuSKeLwd9JdLos-UaYwsLRrMapjOZfJDnkbYbRSKXSTHK6Bl4qbQ9Sv3zFyjtFiPwyU1twuABygL7althKCZX0rwjwhRWN-MRxW8P-hHMiQictQRkrgAaeBjYlOrOtl14
const firebaseConfig = {
  apiKey: "AIzaSyDdIZU-Se8haLnbcsQ1T84VE7hS880mtZs",
  authDomain: "push-ec4db.firebaseapp.com",
  projectId: "push-ec4db",
  storageBucket: "push-ec4db.appspot.com",
  messagingSenderId: "693071420429",
  appId: "1:693071420429:web:041af3a589a362348e4944",
  measurementId: "G-FNDE8JTC35"
};
function App() {
  const [token,setToken] = React.useState("loading token...");
useEffect(()=>{
  async function test(){
    const fapp = initializeApp(firebaseConfig);
    const messaging = getMessaging(fapp);
    
    getToken(messaging, {
      vapidKey:
        "BKUESpVzOHmJHK6JJhNrG2TOR43u-u-DLkxxjey-aKW8D_L9lw5e9WVUjxGU_UlGKb5TkPUaMr8Ee6uwgpCO1V8",
    })
      .then((currentToken) => {
        if (currentToken) {
          console.log("Firebase Token", currentToken);
          setToken(currentToken);
        } else {
          // Show permission request UI
          console.log(
            "No registration token available. Request permission to generate one."
          );
          // ...
        }
      })
      .catch((err) => {
        console.log("An error occurred while retrieving token. ", err);
        // ...
      });
    
  }
  test()
})
  return (
    <div className="App">
     <p>Firebase Token: {token}</p>
    </div>
  );
}

export default App;
