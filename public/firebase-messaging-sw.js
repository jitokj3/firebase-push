 // Scripts for firebase and firebase messaging
 importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
 importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");

 // Initialize the Firebase app in the service worker by passing the generated config
 const firebaseConfig = {
    apiKey: "AIzaSyDdIZU-Se8haLnbcsQ1T84VE7hS880mtZs",
    authDomain: "push-ec4db.firebaseapp.com",
    projectId: "push-ec4db",
    storageBucket: "push-ec4db.appspot.com",
    messagingSenderId: "693071420429",
    appId: "1:693071420429:web:041af3a589a362348e4944",
    measurementId: "G-FNDE8JTC35"
  };
 firebase.initializeApp(firebaseConfig);

 // Retrieve firebase messaging
 const messaging = firebase.messaging();

 messaging.onBackgroundMessage(function(payload) {
   console.log("Received background message ", payload);

   const notificationTitle = payload.notification.title;
   const notificationOptions = {
     body: payload.notification.body,
   };

   self.registration.showNotification(notificationTitle, notificationOptions);
 });